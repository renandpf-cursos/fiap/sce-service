const tsConfig = require("./tsconfig.json");
const tsConfigPaths = require("tsconfig-paths");

const baseUrl = "./dist";
const cleanup = tsConfigPaths.register({
    baseUrl,
    paths: {
        "@gateways/*": ["gateways/*"],
        "@usecases/*": ["use-cases/*"],
        "@domains/*": ["domains/*"],
        "@configs/*": ["configs/*"],       
    },
});
