import "reflect-metadata";
import { mock } from "jest-mock-extended";
import { userJsonFull } from "../../../../__fixture__/json/user.json.template";
import { SaveUserUseCase } from "../../../../../src/use-cases/save-user.usecase";
import { UserController } from "../../../../../src/gateways/http/controllers/user.controller";

describe("Unit Tests of UserController", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const expectedUserId = "anyUserId";
    const userToCreateJson = userJsonFull.build();

    it("Create user with success", async () => {
        const mockedSaveUserUseCase = mock<SaveUserUseCase>();
        mockedSaveUserUseCase.save.mockResolvedValue(expectedUserId);

        const userController = new UserController(mockedSaveUserUseCase);

        const newUserIdPromise = userController.create(userToCreateJson);

        newUserIdPromise.then((newUserId: string) => {
            expect(newUserId).toEqual(expectedUserId);
        });

        const userCaptured = mockedSaveUserUseCase.save.mock.calls[0][0];
        expect(userToCreateJson.id).toEqual(userCaptured.id);
        expect(userToCreateJson.name).toEqual(userCaptured.name);
        expect(userToCreateJson.email).toEqual(userCaptured.email);
        expect(userToCreateJson.password).toEqual(userCaptured.password);
    });
});
