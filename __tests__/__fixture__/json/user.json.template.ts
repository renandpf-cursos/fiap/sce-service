import * as factory from "factory.ts";
//import faker = require("faker");
import faker from "faker";

import { UserJson } from "../../../src/gateways/http/controllers/json/user.json";

export const userJsonFull = factory.Sync.makeFactory<UserJson>({
    id: faker.random.alpha(),
    email: faker.random.alpha(),
    name: faker.random.alpha(),
    password: faker.random.alpha(),
});
