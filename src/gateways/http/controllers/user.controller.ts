import { SaveUserUseCase } from "@usecases/save-user.usecase";
import { Inject } from "@tsed/di";
import { Docs } from "@tsed/swagger";
import { Controller, Post, BodyParams } from "@tsed/common";
import { Description, Summary, Returns, Required } from "@tsed/schema";

import { logger } from "@configs/logger";
import { UserJson } from "./json/user.json";
import { User } from "../../../domain/user";

import { ErrorToAccessDatabaseGatewayException } from "@gateways/exceptions/error-to-access-database-gateway-exception";

@Docs("api-v1")
@Controller("/users")
export class UserController {
    public static TARGET_NAME = "RegistrationController";
    constructor(@Inject(SaveUserUseCase) private saveUserUseCase: SaveUserUseCase) {}

    @Post("/")
    @Summary("Summary of create User")
    @Description("Resource to create User")
    @Returns(201, String)
    @Returns(500, ErrorToAccessDatabaseGatewayException)
    public create(@BodyParams() @Required() userJsonToCreate: UserJson): Promise<string> {
        logger.info("Start", userJsonToCreate);

        const userToCreate = this.mapperUserFromUserJson(userJsonToCreate);

        return this.saveUserUseCase.save(userToCreate);
    }

    private mapperUserFromUserJson(userJson: UserJson): User {
        return new User(userJson.id, userJson.name, userJson.email, userJson.password);
    }
}
