import { Default } from "@tsed/schema";

import { BaseException } from "../../shared/exception/base-exception";

export class ErrorToAccessDatabaseGatewayException extends BaseException {
    @Default("spo.user.errorToAccessDatabase")
    public readonly code: string;

    @Default("Error to access database.")
    public readonly message: string;

    @Default(500)
    public readonly status: number;

    constructor() {
        super();
        this.message = "Error to access database.";
        this.code = "spo.user.errorToAccessDatabase";
        this.status = 500;
    }
}
