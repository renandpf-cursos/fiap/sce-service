import { User } from "src/domain/user";
import { DatabaseGateway } from "../database.gateway";
import { ErrorToAccessDatabaseGatewayException } from "../../exceptions/error-to-access-database-gateway-exception";
import { Injectable, ProviderScope, ProviderType } from "@tsed/common";
import { logger } from "@configs/logger";

@Injectable({
    scope: ProviderScope.SINGLETON,
    type: ProviderType.MIDDLEWARE,
})
export class MongoDataBaseGateway implements DatabaseGateway {
    save(user: User): Promise<string> {
        // TODO: Implementar
        logger.info(user);
        //return Promise.resolve("anyUserId");
        throw new ErrorToAccessDatabaseGatewayException();
    }
}
