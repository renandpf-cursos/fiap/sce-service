import { User } from "src/domain/user";

export interface DatabaseGateway {
    save(user: User): Promise<string>;
}
