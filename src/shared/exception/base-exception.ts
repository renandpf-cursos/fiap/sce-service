import { Description } from "@tsed/schema";

export abstract class BaseException extends Error {
    @Description("Code of error")
    abstract code: string;

    @Description("Message of error")
    abstract message: string;

    @Description("Status http of error")
    abstract status: number;
}
