import "@tsed/platform-express";
import "@tsed/swagger";

import { Configuration } from "@tsed/common";
import { PlatformApplication } from "@tsed/common";
import { Inject } from "@tsed/di";
import { json, urlencoded } from "body-parser";
import { errors } from "celebrate";
import cors from "cors";
import express from "express";
import * as Path from "path";

import "./middlewares/error-middleware";

export const rootDir = Path.resolve(__dirname);

@Configuration({
    acceptMimes: ["application/json"],
    rootDir,
    mount: {
        "/spo/v1": [`${rootDir}/gateways/http/controllers/**/*.ts`],
    },
    swagger: [
        {
            path: "/api-docs-v1",
            doc: "api-v1",
        },
    ],
})
export class App {
    @Inject()
    app: PlatformApplication;

    @Configuration()
    settings: Configuration;

    public $beforeRoutesInit(): void {
        this.app
            .use(express.json())
            .use(json())
            .use(
                urlencoded({
                    extended: true,
                })
            )
            .use(cors())

            .use(errors());
    }
}
