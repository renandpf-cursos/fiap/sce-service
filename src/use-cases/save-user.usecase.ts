import { DatabaseGateway } from "@gateways/database/database.gateway";
import { MongoDataBaseGateway } from "@gateways/database/mongo/mongo-database.gateway";
//import { GATEWAY_TYPES } from "../configs/container/gateway-types";

import { Injectable, Inject, ProviderScope, ProviderType } from "@tsed/common";
import { User } from "src/domain/user";

import { logger } from "@configs/logger";

@Injectable({
    scope: ProviderScope.SINGLETON,
    type: ProviderType.SERVICE,
})
export class SaveUserUseCase {
    constructor(@Inject(MongoDataBaseGateway) private databaseGateway: DatabaseGateway) {}

    public save(user: User): Promise<string> {
        logger.info("Start", user);

        const userIdPromisse = this.databaseGateway.save(user);

        logger.info("end", userIdPromisse);
        return userIdPromisse;
    }
}
