import { PlatformExpress } from "@tsed/platform-express";

import { App } from "./app";
import { logger } from "./configs/logger";
import { properties } from "./configs/properties";

export const startServer = async (httpPort?: number, httpsPort?: number): Promise<any> => {
    try {
        const platform = await PlatformExpress.bootstrap(App, {
            httpPort: httpPort ? httpPort : process.env.PORT ? Number(process.env.PORT) : 7000,
            httpPorts: httpsPort ? httpsPort : process.env.PORT ? Number(process.env.PORT) : 7000,
        });

        await platform.listen();

        logger.info(`🚀 🔥 App listening`);
        //logger.info(`🚀 🔥 App listening on port ${platform.settings.port}`);

        return platform;
    } catch (error) {
        logger.error(`⛑ Server shutdown ${error}`);
    }
};

if (properties.env !== "test") {
    startServer();
}
