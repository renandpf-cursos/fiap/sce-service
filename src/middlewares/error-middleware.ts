import { Catch, PlatformContext, ExceptionFilterMethods, ResponseErrorObject } from "@tsed/common";
import { Exception } from "@tsed/exceptions";

import { logger } from "@configs/logger";

@Catch(Error)
export class HttpExceptionFilter implements ExceptionFilterMethods {
    catch(exception: Exception, context: PlatformContext): void {
        const { response } = context;
        const error = this.mapError(exception);
        const headers = this.getHeaders(exception);

        logger.error({
            message: error.message,
            error: error,
        });

        response.setHeaders(headers).status(error.status).body(error);
    }

    mapError(error: any): any {
        return {
            message: error.message || "Internal Server Error",
            status: error.status || 500,
            code: error.code || "spo.user.internalServerError",
        };
    }

  protected getHeaders(error: any) {
    return [error, error.origin].filter(Boolean).reduce((obj, {headers}: ResponseErrorObject) => {
      return {
        ...obj,
        ...(headers || {})
      };
    }, {});
  }
}
